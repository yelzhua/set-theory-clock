package com.paic.arch.interviews;

public interface TimeConverter {
    String YELLOW_LIGHT = "Y";
    String DIM = "O";
    String RED = "R";
    String Newline_character = "\n";

    String convertTime(String aTime);

}
