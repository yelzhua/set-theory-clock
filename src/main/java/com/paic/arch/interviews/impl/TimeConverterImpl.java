package com.paic.arch.interviews.impl;

import com.paic.arch.interviews.TimeConverter;

public class TimeConverterImpl implements TimeConverter {

    public static final int HOUR_5 = 5;

    public static final int HOUR_NUM = 4;

    public static final int MINUTE_NUM_UP = 11;

    public static final int MINUTE_NUM_DOWN = 4;

    @Override
    public String convertTime(String aTime) {
        try {
            // 对字符串做拆解
            String[] timeArr = aTime.split(":", 3);
            if (timeArr.length < 3) {
                return aTime;
            }
            // 解析出时分秒
            Integer _hour = Integer.valueOf(removeZero(timeArr[0]));
            Integer _minute = Integer.valueOf(removeZero(timeArr[1]));
            Integer _second = Integer.valueOf(removeZero(timeArr[2]));
            // 时分秒校验
            if (_hour < 0 || _hour > 24) {
                return aTime;
            }
            if (_minute < 0 || _minute > 59) {
                return aTime;
            }
            if (_second < 0 || _second > 59) {
                return aTime;
            }
            StringBuilder sb = new StringBuilder();
            // 先解析最顶上每两秒亮一次的灯
            // 如果秒钟是偶数，顶上亮，否则灭
            if (_second % 2 == 0) {
                sb.append(YELLOW_LIGHT);
            } else {
                sb.append(DIM);
            }
            // 添加换行符
            sb.append(Newline_character);
            // 解析小时，设置上面两行的展示 ：比如 13点
            int aHour1 = _hour / HOUR_5; // 2
            int aHour2 = _hour % HOUR_5; // 3
            // 设置第一行亮灯数
            for (int i = 0; i < aHour1; i++) {
                sb.append(RED);
            }
            // 设置第一行暗灯数
            for (int i = 0; i < HOUR_NUM - aHour1; i++) {
                sb.append(DIM);
            }
            // 换行
            sb.append(Newline_character);
            // 设置第二行
            // 设置第二行亮灯数
            for (int i = 0; i < aHour2; i++) {
                sb.append(RED);
            }
            // 设置第二行暗灯数
            for (int i = 0; i < HOUR_NUM - aHour2; i++) {
                sb.append(DIM);
            }
            // 换行
            sb.append(Newline_character);

            // 设置分钟的展示 如 57
            int aMinute1 = _minute / HOUR_5;// 11
            int aMinute2 = _minute % HOUR_5;// 2
            // 设置第三行
            for (int i = 1; i <= aMinute1; i++) {
                if (i % 3 == 0) {
                    sb.append(RED);
                } else {
                    sb.append(YELLOW_LIGHT);
                }
            }
            for (int i = 0; i < MINUTE_NUM_UP - aMinute1; i++) {
                sb.append(DIM);
            }
            sb.append(Newline_character);
            // 设置第四行
            // 设置第4行亮灯数
            for (int i = 0; i < aMinute2; i++) {
                sb.append(YELLOW_LIGHT);
            }
            // 设置第4行暗灯数
            for (int i = 0; i < MINUTE_NUM_DOWN - aMinute2; i++) {
                sb.append(DIM);
            }
            return sb.toString();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return aTime;
    }

    /**
     * 去掉开头的0
     * 
     * @param str
     * @return
     *
     * @author {lizhenhua} 2018年4月10日 下午9:49:01
     */
    private String removeZero(String str) {
        if (str.startsWith("0")) {
            return str.substring(1);
        }
        return str;
    }

}
